﻿namespace RecruitmentLib
{

  public interface RecruitmentItem
  {
    int Id { get; }
  }

  public class Recruitment
  {
    private CustomList<Customer> customers;
    private CustomList<Unemployed> unemployments;
    private CustomList<Category> categories;

    public Recruitment()
    {
      customers = new CustomList<Customer>();
      unemployments = new CustomList<Unemployed>();
      categories = new CustomList<Category>();
    }

    public Category CreateCategory(string name)
    {
      Category newCategory = new Category(name);
      categories.Add(newCategory);
      return newCategory;
    }

    public void Add(RecruitmentItem item)
    {
      if (item is Customer)
        customers.Add((Customer)item);
      else if (item is Unemployed)
        unemployments.Add((Unemployed)item);
      else if (item is Category)
        categories.Add((Category)item);
    }

    public void Remove(int id)
    {
      customers.Remove(v => v.Id == id);
      unemployments.Remove(r => r.Id == id);
      categories.Remove(c => c.Id == id);
    }

    public void Update(int id, RecruitmentItem newItem)
    {
      Remove(id);
      Add(newItem);
    }

    public CustomList<Unemployed> SortUnemploymentsBy(string sortBy)
    {
      var prop = typeof(Unemployed).GetProperty(sortBy);
      if (prop == null)
      {
        throw new ArgumentException("Invalid field name for sorting");
      }

      return unemployments.OrderBy(item => prop.GetValue(item));
    }

    public CustomList<Customer> SortCustomersBy(string sortBy)
    {
      var prop = typeof(Unemployed).GetProperty(sortBy);
      if (prop == null)
      {
        throw new ArgumentException("Invalid field name for sorting");
      }

      return customers.OrderBy(item => prop.GetValue(item));
    }

    public CustomList<Unemployed> GetAllUnemployments()
    {
      return unemployments;
    }

    public CustomList<Customer> GetAllCustomers()
    {
      return customers;
    }

    public RecruitmentItem? Find(int id)
    {
      var customer = customers.Find(e => e.Id == id);
      var unemployed = unemployments.Find(e => e.Id == id);
      var category = categories.Find(e => e.Id == id);

      return customer != null ? customer :
             unemployed != null ? unemployed :
             category != null ? category : null;
    }

    public CustomList<Unemployed> findUnemploymentsByKeyWord(string keyWord)
    {
      CustomList<Unemployed> foundUnemployments = new CustomList<Unemployed>();

      foreach (var unemployed in unemployments)
      {
        if (unemployed.FirstName.Contains(keyWord) ||
          unemployed.LastName.Contains(keyWord) ||
          unemployed.Education.Contains(keyWord) ||
          unemployed.LastJob.Contains(keyWord) ||
          unemployed.GetSkills().Contains(keyWord))
        {
          foundUnemployments.Add(unemployed);
        }

      }

      return foundUnemployments;
    }
  }
}
