namespace RecruitmentLib
{
  public class Customer : User
  {
    public string Company { get; set; }
    private double Budget { get; set; }

    private CustomList<Vacancy> Vacancies;

    public Customer(string firstName, string lastName, string company, double budget) : base(firstName, lastName)
    {
      Id = GenerateId();
      FirstName = firstName;
      LastName = lastName;
      Company = company;
      Budget = budget;
      Vacancies = new CustomList<Vacancy>();
    }

    public Vacancy createVacancy(string occupation, string requirements, string location, decimal salary)
    {
      Vacancy newVacancy = new Vacancy(occupation, requirements, location, salary);
      Vacancies.Add(newVacancy);
      return newVacancy;
    }
    public CustomList<Vacancy> GetVacancies()
    {
      return Vacancies;
    }
    public void UpdateBudget(double newBudget)
    {
      Budget = newBudget;
    }

    public double GetBudget()
    {
      return Budget;
    }

    public override string ToString()
    {
      return $"Customer Details:\tId: {Id}\tFirstName: {FirstName}\t LastName: {LastName}\tCompany: {Company}\tBudget: {GetBudget()}";
    }
  }

}