namespace RecruitmentLib
{
  public interface CategoryItem
  {
    int Id { get; }
  }

  public class Category : RecruitmentItem
  {
    public int Id { get; set; }
    public string Name { get; set; }
    private CustomList<Vacancy> Vacancies { get; set; }
    private CustomList<Resume> Resumes { get; set; }

    public Category(string name)
    {
      Id = GenerateId();
      Name = name;
      Vacancies = new CustomList<Vacancy>();
      Resumes = new CustomList<Resume>();
    }

    public void Add(CategoryItem item)
    {
      if (item is Vacancy vacancy)
      {
        Vacancies.Add(vacancy);
      }
      else if (item is Resume resume)
      {
        Resumes.Add(resume);
      }
    }

    public void Remove(int id)
    {
      Vacancies.Remove(v => v.Id == id);
      Resumes.Remove(r => r.Id == id);
    }

    public void Update(int id, CategoryItem item)
    {
      Remove(id);
      Add(item);
    }

    public CustomList<Vacancy> SortVacanciesBy(string sortBy)
    {
      CustomList<Vacancy> result = new CustomList<Vacancy>();

      if (sortBy == "Salary")
      {
        result = Vacancies.OrderBy(item => item.GetSalary());
      }
      else
      {
        var prop = typeof(Vacancy).GetProperty(sortBy);
        if (prop == null)
        {
          throw new ArgumentException("Invalid field name for sorting");
        }
        result = Vacancies.OrderBy(item => prop.GetValue(item));
      }

      return result;
    }

    public CustomList<Resume> SortResumesBy(string sortBy)
    {
      CustomList<Resume> result = new CustomList<Resume>();

      if (sortBy == "Certification")
      {
        result = Resumes.OrderBy(item => item.GetCertification());
      }
      else
      {
        var prop = typeof(Resume).GetProperty(sortBy);
        if (prop == null)
        {
          throw new ArgumentException("Invalid field name for sorting");
        }
        result = Resumes.OrderBy(item => prop.GetValue(item));
      }

      return result;
    }

    public CustomList<Vacancy> GetAllVacancies()
    {
      return Vacancies;
    }

    public CustomList<Resume> GetAllResumes()
    {
      return Resumes;
    }

    public CategoryItem? Find(int id)
    {
      var vacancy = Vacancies.Find(e => e.Id == id);
      var resume = Resumes.Find(e => e.Id == id);

      return vacancy != null ? vacancy :
             resume != null ? resume : null;
    }
    public CustomList<Vacancy> findVacanciesByKeyWord(string keyWord)
    {
      CustomList<Vacancy> foundVacancies = new CustomList<Vacancy>();

      foreach (var vacancy in Vacancies)
      {
        if (vacancy.Occupation.Contains(keyWord) ||
          vacancy.Requirements.Contains(keyWord) ||
          vacancy.Location.Contains(keyWord) ||
          vacancy.GetSalary().ToString().Contains(keyWord))
        {
          foundVacancies.Add(vacancy);
        }

      }

      return foundVacancies;
    }

    public int GenerateId()
    {
      Random random = new Random();

      return random.Next(1000, 100000);
    }
  }
}