namespace RecruitmentLib
{
  public abstract class User : RecruitmentItem
  {
    public int Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Phone { get; set; }

    public User(string firstName, string lastName)
    {
      FirstName = firstName;
      LastName = lastName;
      Phone = "";
    }

    public void ChangeName(string firstName, string lastName)
    {
      FirstName = firstName;
      LastName = lastName;
    }

    public void UpdatePhone(string phone)
    {
      Phone = phone;
    }

    public int GenerateId()
    {
      Random random = new Random();

      return random.Next(1000, 100000);
    }
  }

}