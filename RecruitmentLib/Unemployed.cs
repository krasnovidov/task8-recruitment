namespace RecruitmentLib
{
  public class Unemployed : User
  {
    public string Education { get; set; }
    public string LastJob { get; set; }
    private string Skills { get; set; }
    private CustomList<Resume> Resumes;

    public Unemployed(string firstName, string lastName, string education, string lastJob, string skills) : base(firstName, lastName)
    {
      Id = GenerateId();
      FirstName = firstName;
      LastName = lastName;
      Education = education;
      LastJob = lastJob;
      Skills = skills;
      Resumes = new CustomList<Resume>();
    }
    public Resume createResume(string occupation, string experience, string certification)
    {
      Resume newResume = new Resume(occupation, experience, certification);
      Resumes.Add(newResume);
      return newResume;
    }
    public CustomList<Resume> GetResumes()
    {
      return Resumes;
    }
    public void UpdateSkills(string newSkills)
    {
      Skills = newSkills;
    }

    public string GetSkills()
    {
      return Skills;
    }

    public override string ToString()
    {
      return $"Unemployed Details:\tId: {Id}\tFirstName: {FirstName}\t LastName: {LastName}\tEducation: {Education}\tLastJob: {LastJob}\tSkills: {GetSkills()}";
    }
  }

}