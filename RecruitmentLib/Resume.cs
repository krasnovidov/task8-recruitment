namespace RecruitmentLib
{
  public class Resume : CategoryItem
  {
    public int Id { get; }
    public string Occupation { get; set; }
    public string Experience { get; set; }
    private string Certification { get; set; }

    public Resume(string occupation, string experience, string certification)
    {
      Id = GenerateId();
      Occupation = occupation;
      Experience = experience;
      Certification = certification;
    }
    public void UpdateCertification(string newCertification)
    {
      Certification = newCertification;
    }

    public string GetCertification()
    {
      return Certification;
    }

    public override string ToString()
    {
      return $"Resume Details:\tId: {Id}\tOccupation: {Occupation}\tExperience: {Experience}\tCertification: {GetCertification()}";
    }

    private int GenerateId()
    {
      Random random = new Random();

      return random.Next(1000, 100000);
    }
  }

}