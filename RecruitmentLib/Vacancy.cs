namespace RecruitmentLib
{
  public class Vacancy : CategoryItem
  {
    public int Id { get; }
    public string Occupation { get; set; }
    public string Requirements { get; set; }
    public string Location { get; set; }
    private decimal Salary { get; set; }

    public Vacancy(string occupation, string requirements, string location, decimal salary)
    {
      Id = GenerateId();
      Occupation = occupation;
      Requirements = requirements;
      Location = location;
      Salary = salary;
    }

    public void SetSalary(decimal newSalary)
    {
      Salary = newSalary;
    }

    public decimal GetSalary()
    {
      return Salary;
    }

    public override string ToString()
    {
      return $"Vacancy Details:\tId: {Id}\tOccupation: {Occupation}\tRequirements: {Requirements}\tLocation: {Location}\tSalary: {GetSalary()}";
    }

    private int GenerateId()
    {
      Random random = new Random();

      return random.Next(1000, 100000);
    }
  }

}