﻿namespace RecruitmentLib
{
  public class CustomList<T>
  {
    private Node<T>? head;
    private int count;

    public CustomList()
    {
      head = null;
      count = 0;
    }

    public int Count
    {
      get { return count; }
    }

    public void Add(T item)
    {
      if (head == null)
      {
        head = new Node<T>(item);
      }
      else
      {
        Node<T> current = head;
        while (current.Next != null)
        {
          current = current.Next;
        }
        current.Next = new Node<T>(item);
      }
      count++;
    }

    public void Remove(Predicate<T> match)
    {
      while (head != null && match(head.Value))
      {
        head = head.Next;
        count--;
      }

      if (head != null)
      {
        Node<T> current = head;
        while (current.Next != null)
        {
          if (match(current.Next.Value))
          {
            current.Next = current.Next.Next;
            count--;
          }
          else
          {
            current = current.Next;
          }
        }
      }

    }

    public IEnumerator<T> GetEnumerator()
    {
      Node<T>? current = head;
      while (current != null)
      {
        yield return current.Value;
        current = current.Next;
      }
    }

    public CustomList<T> OrderBy<TKey>(Func<T, TKey> keySelector)
    {
      CustomList<T> sortedCustomList = new CustomList<T>();

      Node<T>? current = head;
      while (current != null)
      {
        sortedCustomList.Add(current.Value);
        current = current.Next;
      }

      bool swapped;
      do
      {
        swapped = false;
        current = sortedCustomList.head;
        while (current != null && current.Next != null)
        {
          if (Comparer<TKey>.Default.Compare(keySelector(current.Value), keySelector(current.Next.Value)) > 0)
          {
            T temp = current.Value;
            current.Value = current.Next.Value;
            current.Next.Value = temp;
            swapped = true;
          }
          current = current.Next;
        }
      } while (swapped);

      return sortedCustomList;
    }

    public T? Find(Predicate<T> match)
    {
      Node<T>? current = head;
      while (current != null)
      {
        if (match(current.Value))
        {
          return current.Value;
        }
        current = current.Next;
      }
      return default;
    }

    public T this[int index]
    {
      get
      {
        if (index < 0 || index >= count)
        {
          throw new IndexOutOfRangeException();
        }
        Node<T>? current = head;
        for (int i = 0; i < index; i++)
        {
          if (current != null)
          {
            current = current.Next;
          }
        }
        if (current == null)
        {
          throw new InvalidOperationException("Node at index is null");
        }
        return current.Value;
      }
      set
      {
        if (index < 0 || index >= count)
        {
          throw new IndexOutOfRangeException();
        }
        Node<T>? current = head;
        for (int i = 0; i < index; i++)
        {
          if (current != null)
          {
            current = current.Next;
          }
        }
        if (current == null)
        {
          throw new InvalidOperationException("Node at index is null");
        }
        current.Value = value;
      }
    }
  }
}
