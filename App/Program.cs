﻿using System.Diagnostics;
using RecruitmentLib;

namespace App
{
  class Program
  {
    static void Main()
    {
      Console.WriteLine("Hello, world!");

      Console.WriteLine("1. Управління вакансіями і резюме");
      vacancyAndResumeManaging();

      Console.WriteLine("2. Управління безробітними");
      unemploymentsManaging();

      Console.WriteLine("3. Управління фірмами замовниками");
      customersManaging();

      Console.WriteLine("4. Пошук");
      search();


      Console.WriteLine("The end of program");
    }

    public static void search()
    {
      Recruitment recruitment1 = new Recruitment();
      Category category1 = recruitment1.CreateCategory("IT industry");
      Customer customer1 = new Customer("Lukas", "De Holland", "Apple", 720000);

      Vacancy vacancy1 = customer1.createVacancy("Developer", "React", "Kyiv", 500);
      Vacancy vacancy2 = customer1.createVacancy("Engineer", "React and Angular", "Berlin", 600);
      Vacancy vacancy3 = customer1.createVacancy("Designer", "Adobe XD", "New York", 700);
      Vacancy vacancy4 = customer1.createVacancy("Manager", "Project Management", "London", 800);
      Vacancy vacancy5 = customer1.createVacancy("Data Scientist", "Python", "San Francisco", 900);

      category1.Add(vacancy1);
      category1.Add(vacancy2);
      category1.Add(vacancy3);
      category1.Add(vacancy4);
      category1.Add(vacancy5);

      Console.WriteLine("4.1. Можливість пошуку по ключовому слову серед вакансій");
      CustomList<Vacancy> vacancies = category1.findVacanciesByKeyWord("React");
      foreach (var vacancy in vacancies)
      {
        Console.WriteLine(vacancy.ToString());
      }

      Unemployed unemployed1 = new Unemployed("Nick", "Morgan", "Harvard Uni", "Professor", "Javascript and teaching");
      Unemployed unemployed2 = new Unemployed("Alice", "Smith", "MIT", "Researcher", "Data analysis");
      Unemployed unemployed3 = new Unemployed("John", "Doe", "Stanford", "Engineer", "Javascript and Software development");
      Unemployed unemployed4 = new Unemployed("Emma", "Johnson", "Oxford", "Writer", "Creative writing");
      Unemployed unemployed5 = new Unemployed("Michael", "Brown", "Cambridge", "Consultant", "Business analysis");

      recruitment1.Add(unemployed1);
      recruitment1.Add(unemployed2);
      recruitment1.Add(unemployed3);
      recruitment1.Add(unemployed4);
      recruitment1.Add(unemployed5);

      Console.WriteLine("4.2. Можливість пошуку по ключовому слову серед безробітних");
      CustomList<Unemployed> unemployments = recruitment1.findUnemploymentsByKeyWord("Javascript");
      foreach (var unemployed in unemployments)
      {
        Console.WriteLine(unemployed.ToString());
      }
    }

    public static void customersManaging()
    {
      Recruitment recruitment1 = new Recruitment();

      Customer customer1 = new Customer("Lukas", "De Holland", "Apple", 720000);
      Customer customer2 = new Customer("Sophia", "Garcia", "Microsoft", 550000);
      Customer customer3 = new Customer("Daniel", "Chen", "Google", 900000);
      Customer customer4 = new Customer("Emily", "Wilson", "Amazon", 480000);
      Customer customer5 = new Customer("James", "Nguyen", "Facebook", 680000);

      Console.WriteLine("3.1. Можливість додавати замовників");
      recruitment1.Add(customer1);
      recruitment1.Add(customer2);
      recruitment1.Add(customer3);
      recruitment1.Add(customer4);
      recruitment1.Add(customer5);
      Debug.WriteLine(recruitment1.Find(customer1.Id) == customer1, "customer added to recruitment1");

      Console.WriteLine("3.2. Можливість видаляти замовників");
      recruitment1.Remove(customer1.Id);
      Debug.WriteLine(recruitment1.Find(customer1.Id) == null, "customer removed from recruitment1");

      Console.WriteLine("3.3. Можливість змінювати дані замовників");
      customer2.Company = "Minisoft";
      customer3.FirstName = "John";
      customer4.LastName = "Ndusen";
      customer5.UpdateBudget(234752);
      Debug.WriteLine(customer2.Company == "Minisoft", "Company updated");
      Debug.WriteLine(customer3.FirstName == "John", "FirstName updated");
      Debug.WriteLine(customer4.LastName == "Ndusen", "LastName updated");
      Debug.WriteLine(customer5.GetBudget() == 234752, "Budget updated");

      Console.WriteLine("3.4. Можливість переглянути дані конкретного замовника");
      Customer foundCustomer = (Customer)recruitment1.Find(customer2.Id);
      Console.WriteLine(foundCustomer.ToString());

      Console.WriteLine("3.5. Можливість переглянути список всіх замовників");
      CustomList<Customer> customers = recruitment1.GetAllCustomers();
      foreach (var customer in customers)
      {
        Console.WriteLine(customer.ToString());
      }

      Console.WriteLine("3.5.1. Можливість відсортувати список по імені");
      customers = recruitment1.SortCustomersBy("FirstName");
      foreach (var customer in customers)
      {
        Console.WriteLine(customer.ToString());
      }

      Console.WriteLine("3.5.2. Можливість відсортувати список по прізвищу");
      customers = recruitment1.SortCustomersBy("LastName");
      foreach (var customer in customers)
      {
        Console.WriteLine(customer.ToString());
      }
    }
    public static void unemploymentsManaging()
    {
      Recruitment recruitment1 = new Recruitment();

      Unemployed unemployed1 = new Unemployed("Nick", "Morgan", "Harvard Uni", "Professor", "Javascript and teaching");
      Unemployed unemployed2 = new Unemployed("Alice", "Smith", "MIT", "Researcher", "Data analysis");
      Unemployed unemployed3 = new Unemployed("John", "Doe", "Stanford", "Engineer", "Javascript and Software development");
      Unemployed unemployed4 = new Unemployed("Emma", "Johnson", "Oxford", "Writer", "Creative writing");
      Unemployed unemployed5 = new Unemployed("Michael", "Brown", "Cambridge", "Consultant", "Business analysis");

      Console.WriteLine("2.1. Можливість додавати безробітних");
      recruitment1.Add(unemployed1);
      recruitment1.Add(unemployed2);
      recruitment1.Add(unemployed3);
      recruitment1.Add(unemployed4);
      recruitment1.Add(unemployed5);
      Debug.WriteLine(recruitment1.Find(unemployed1.Id) == unemployed1, "unemployed added to recruitment1");

      Console.WriteLine("2.2. Можливість видаляти безробітних");
      recruitment1.Remove(unemployed1.Id);
      Debug.WriteLine(recruitment1.Find(unemployed1.Id) == null, "unemployed removed from recruitment1");

      Console.WriteLine("2.3. Можливість змінювати дані безробітних");
      unemployed2.LastJob = "Doctor";
      unemployed3.FirstName = "Nick";
      unemployed4.LastName = "Ndusen";
      unemployed5.UpdateSkills("Teaching");
      Debug.WriteLine(unemployed2.LastJob == "Doctor", "LastJob updated");
      Debug.WriteLine(unemployed3.FirstName == "Nick", "FirstName updated");
      Debug.WriteLine(unemployed4.LastName == "Ndusen", "LastName updated");
      Debug.WriteLine(unemployed5.GetSkills() == "Teaching", "Skills updated");

      Console.WriteLine("2.4. Можливість переглянути дані конкретного безробітного");
      Unemployed foundUnemployed = (Unemployed)recruitment1.Find(unemployed4.Id);
      Console.WriteLine(foundUnemployed.ToString());

      Console.WriteLine("2.5. Можливість переглянути список всіх безробітних");
      CustomList<Unemployed> unemployments = recruitment1.GetAllUnemployments();
      foreach (var unemployed in unemployments)
      {
        Console.WriteLine(unemployed.ToString());
      }

      Console.WriteLine("2.5.1. Можливість відсортувати список по імені");
      unemployments = recruitment1.SortUnemploymentsBy("FirstName");
      foreach (var unemployed in unemployments)
      {
        Console.WriteLine(unemployed.ToString());
      }

      Console.WriteLine("2.5.2. Можливість відсортувати список по прізвищу");
      unemployments = recruitment1.SortUnemploymentsBy("LastName");
      foreach (var unemployed in unemployments)
      {
        Console.WriteLine(unemployed.ToString());
      }
    }

    public static void vacancyAndResumeManaging()
    {
      Recruitment recruitment1 = new Recruitment();
      Category Category1 = recruitment1.CreateCategory("IT industry");
      Customer customer1 = new Customer("Lukas", "De Holland", "Apple", 720000);
      Unemployed unemployed1 = new Unemployed("Nick", "Morgan", "Harvard Uni", "Professor", "Javascript and teaching");

      Vacancy vacancy1 = customer1.createVacancy("Developer", "React", "Kyiv", 500);
      Vacancy vacancy2 = customer1.createVacancy("Engineer", "React and Angular", "Berlin", 600);
      Vacancy vacancy3 = customer1.createVacancy("Designer", "Adobe XD", "New York", 700);
      Vacancy vacancy4 = customer1.createVacancy("Manager", "Project Management", "London", 800);
      Vacancy vacancy5 = customer1.createVacancy("Data Scientist", "Python", "San Francisco", 900);
      Resume resume1 = unemployed1.createResume("Software Engineer", "5 years", "Microsoft Certified");
      Resume resume2 = unemployed1.createResume("Data Analyst", "3 years", "SQL Certified");
      Resume resume3 = unemployed1.createResume("Graphic Designer", "2 years", "Adobe Certified");
      Resume resume4 = unemployed1.createResume("Project Manager", "7 years", "PMP Certified");
      Resume resume5 = unemployed1.createResume("Network Administrator", "4 years", "Cisco Certified");

      Console.WriteLine("1.1. Можливість додавати вакансію і резюме до категорії");
      Category1.Add(vacancy1);
      Category1.Add(vacancy2);
      Category1.Add(vacancy3);
      Category1.Add(vacancy4);
      Category1.Add(vacancy5);
      Category1.Add(resume1);
      Category1.Add(resume2);
      Category1.Add(resume3);
      Category1.Add(resume4);
      Category1.Add(resume5);

      Debug.WriteLine(Category1.Find(vacancy1.Id) == vacancy1, "vacancy added to category1");
      Debug.WriteLine(Category1.Find(resume1.Id) == resume1, "resume added to category1");

      Console.WriteLine("1.2. Можливість видаляти вакансію і резюме з категорії");
      Category1.Remove(vacancy1.Id);
      Category1.Remove(resume1.Id);
      Debug.WriteLine(Category1.Find(resume1.Id) == null, "resume removed from category1");
      Debug.WriteLine(Category1.Find(vacancy1.Id) == null, "vacancy removed from category1");

      Console.WriteLine("1.3. Можливість змінювати дані вакансій і резюме");
      resume3.UpdateCertification("Super certification");
      resume4.Occupation = "Teacher";
      vacancy2.Location = "Dnipro";
      vacancy3.SetSalary(12003);
      vacancy4.SetSalary(int.MaxValue);

      Debug.WriteLine(vacancy2.Location == "Dnipro", "location updated");
      Debug.WriteLine(vacancy3.GetSalary() == 12003, "salary updated");
      Debug.WriteLine(vacancy4.GetSalary() == int.MaxValue, "salary updated");
      Debug.WriteLine(resume3.GetCertification() == "Super certification", "certification updated");
      Debug.WriteLine(resume4.Occupation == "Teacher", "occupation updated");

      Console.WriteLine("1.4. Можливість переглянути дані конкретної вакансії і резюме");
      Resume foundResume = (Resume)Category1.Find(resume2.Id);
      Vacancy foundVacancy = (Vacancy)Category1.Find(vacancy2.Id);
      Console.WriteLine(foundResume.ToString());
      Console.WriteLine(foundVacancy.ToString());

      Console.WriteLine("1.5. Можливість переглянути відсортований список всіх вакансій");
      CustomList<Resume> resumes = Category1.GetAllResumes();
      CustomList<Vacancy> vacancies = Category1.GetAllVacancies();

      Console.WriteLine("RESUMES BEFORE SORTING");
      foreach (var resume in resumes)
      {
        Console.WriteLine(resume.ToString());
      }

      Console.WriteLine("RESUMES AFTER SORTING");
      resumes = Category1.SortResumesBy("Id");
      foreach (var resume in resumes)
      {
        Console.WriteLine(resume.ToString());
      }

      Console.WriteLine("1.6. Можливість переглянути відсортований список всіх резюме");
      Console.WriteLine("VACANCIES BEFORE SORTING");
      foreach (var vacancy in vacancies)
      {
        Console.WriteLine(vacancy.ToString());
      }

      Console.WriteLine("VACANCIES AFTER SORTING");
      vacancies = Category1.SortVacanciesBy("Salary");
      foreach (var vacancy in vacancies)
      {
        Console.WriteLine(vacancy.ToString());
      }
    }
  }
}